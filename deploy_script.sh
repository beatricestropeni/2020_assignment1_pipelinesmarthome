#!/bin/bash

set -xe
## Connessione tramite ssh alla VM. L'immagine Docker viene acquisita dal Container registry di Gitlab e eseguita sulla VM.
ssh -o PreferredAuthentications=publickey $DEPLOY_USERNAME@$DEPLOY_IP \
"docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY \
&& docker rm --force smarthome2 || true \
&& docker pull $IMAGE_NAME \
&& docker run -d --restart on-failure:5 --name smarthome2 $IMAGE_NAME \
&& exit"
