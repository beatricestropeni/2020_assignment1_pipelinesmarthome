FROM openjdk:11-jdk-slim

WORKDIR /usr/app
COPY ./target/SmartHome2-0.0.1-SNAPSHOT.jar ./smarthome.jar

EXPOSE 8080
ENTRYPOINT ["java", "-jar", "smarthome.jar"]
