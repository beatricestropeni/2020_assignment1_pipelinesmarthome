# Assignment 1 - Corso di Processo e Sviluppo del Software

## Membri

- Beatrice Stropeni 830017
- Alessandra Rota 829775
- Sofia Zonari 829741

## Contenuto

Il progetto consiste in creare una pipeline per SmartHome, precedentemente creato in un corso della triennale. L'applicazione SmartHome si occupa di gestire la componente di domotica della casa. Consiste in un interfaccia che permette di visualizzare lo stato attuale della casa e di visionare ogni stanza con i relativi oggetti. I componenti possibili con cui interagire sono: 
- finestre
- tapparelle
- lampade
- lavatrice
- lavastoviglie
- robot delle pulizie

Inoltre è previsto un terminale per il controllo dell'allarme casalingo; i dispositivi collegati ad esso sono: sensori di movimento, sensori di effrazione e sensore delle fughe di gas. 

## Considerazioni

Dato che il progetto era stato sviluppato con il supporto di Maven, abbiamo deciso di mantenere questa tecnologia nella pipeline. In seguito riportiamo alcune considerazioni per ogni stage della pipeline:

- Build: ci siamo appoggiati a Maven e abbiamo utilizzato una sua immagine.
- Verify: ci siamo serviti dei plugin Spotbugs e Checkstyle per identificare rispettivamente i bug e i code smell del codice. Per il primo abbiamo deciso di non considerare i bug trovati in modo da proseguire nella pipeline. 
- Test: abbiamo usufruito dei plugin maven Surefire e Failsafe per distinguere unit test e integration test.
- Package: ci siamo appoggiati direttamente ai comandi Maven.
- Release: abbiamo utilizzato Docker in Docker e l'immagine prodotta è stata caricata su GitLab Container Registry. 
- Deploy: tramite collegamento ssh ci siamo appoggiati ad un server privato, utilizzando un'immagine di Debian.

Inoltre abbiamo scelto di implementare una collezione di cache, suddivise per branch e stage della pipeline. Gli unici stage che non usufruiscono di questa tecnologia sono quelli di release e deploy, dato che entrambi non hanno nessun tipo di dipendenza da risolvere.

## Problemi

Durante l'intero sviluppo della pipeline siamo incappate in svariati piccoli problemi che spesso hanno rallentato il lavoro, sopratutto in fase di deploy. 
Inizialmente volevamo utilizzare Amazon Web Services ma abbiamo avuto difficoltà a capire come impostare le varie caratteristiche richieste per potersi collegare via ssh. Avremmo probabilmente potuto continuare con questa scelta se avessimo avuto più tempo; perciò abbiamo deciso di optare per un deploy su una macchina privata, gentilmente offerta da un nostro collega di corso.

## Link al repository

https://gitlab.com/beatricestropeni/2020_assignment1_pipelinesmarthome